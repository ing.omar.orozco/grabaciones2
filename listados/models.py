# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class grabaciones(models.Model):
    name = models.CharField(max_length=200,verbose_name='nombre')
    anio = models.CharField(max_length=200,verbose_name='año',null=True, blank=True)
    mes = models.CharField(max_length=200,verbose_name='mes',null=True, blank=True)
    dia = models.CharField(max_length=200,verbose_name='dia',null=True, blank=True)
    ruta = models.CharField(max_length=200,verbose_name='ruta',null=True, blank=True)
    hora = models.CharField(max_length=200,verbose_name='hora',null=True, blank=True)
    fecha =  models.DateTimeField(verbose_name="fecha",null=True, blank=True)
    class Meta:
        verbose_name = 'Listado de Grabaciones'
        verbose_name_plural = 'Listado de Grabaciones'
        unique_together = ('name','ruta',)

    def __str__(self):
        return self.name