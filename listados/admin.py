# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe

# Register your models here.
class grabacionesAdmin(admin.ModelAdmin):
    list_display = ("name","anio","mes","dia","hora","link_descarga")
    list_filter = ("anio","mes","dia","hora","fecha")
    def link_descarga(self, obj):
        # return format_html_join(
        #     mark_safe('<br>'),
        #     '{}',
        #     ((line,) for line in instance.get_full_address()),
        # ) or mark_safe("<span class='errors'>I can't determine this address.</span>")
        # return mark_safe('<a href="/descargar/%s">Reproducir</a>' % obj.id)
        return mark_safe('<audio controls><source src="/descargar/%s" type="audio/wav"></audio>' % obj.id)
    link_descarga.allow_tags = True
    link_descarga.short_description = 'Descargas'
admin.site.register(grabaciones,grabacionesAdmin)