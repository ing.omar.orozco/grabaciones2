# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import *
from django.http import HttpResponse

# Create your views here.
from django.utils.encoding import smart_str

def Descargar(request,id):
    archivo = grabaciones.objects.get(pk=id)
    # print(archivo)
    # response = HttpResponse(mimetype='application/force-download') # mimetype is replaced by content_type for django 1.7
    # response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(archivo.name)

    # response['X-Sendfile'] = smart_str(archivo.ruta)
    # return response

    my_file = open(str(archivo.ruta),'rb').read()
    return HttpResponse(my_file, content_type = "audio/wav")
# It's usually a good idea to set the 'Content-Length' header too.
# You can also set any other required headers: Cache-Control, etc.